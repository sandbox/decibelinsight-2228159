<?php

/**
 * @file
 * Allows you to add your Decibel Insight tracking code to your site
 * This project is a modified version of the add_to_head module which 
 * can be found here: https://drupal.org/project/add_to_head
 */

/**
 * Implements hook_menu().
 */
function decibel_insight_integration_menu() {
  $items = array();

  $items['admin/config/development/Decibel-Insight'] = array(
    'title' => 'Decibel Insight',
    'description' => 'Configure <em>Decibel Insight</em>.',
    'page callback' => 'decibel_insight_integration_overview',
    'file' => 'decibel_insight_integration.admin.inc',
    'access arguments' => array('administer Decibel Insight'),
  );

  $items['admin/config/development/Decibel-Insight/add'] = array(
    'title' => 'Add New Profile',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('decibel_insight_integration_add_profile'),
    'file' => 'decibel_insight_integration.admin.inc',
    'access arguments' => array('administer Decibel Insight'),
    'type' => MENU_CALLBACK,
  );

  $items['admin/config/development/Decibel-Insight/%decibel_insight_integration_profile'] = array(
    'title' => 'Edit Profile',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('decibel_insight_integration_edit_profile', 4),
    'file' => 'decibel_insight_integration.admin.inc',
    'access arguments' => array('administer Decibel Insight'),
    'type' => MENU_CALLBACK,
  );

  $items['admin/config/development/Decibel-Insight/%decibel_insight_integration_profile/delete'] = array(
    'title' => 'Delete Profile',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('decibel_insight_integration_delete_profile_confirm', 4),
    'file' => 'decibel_insight_integration.admin.inc',
    'access arguments' => array('administer Decibel Insight'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}


/**
 * Implements hook_forms().
 */
function decibel_insight_integration_forms($form_id, $arg) {
  return array(
    'decibel_insight_integration_add_profile' => array(
      'callback' => 'decibel_insight_integration_edit_profile',
    ),
  );
}


/**
 * Implements hook_permission().
 */
function decibel_insight_integration_permission() {
  return array(
    'administer Decibel Insight' =>  array(
      'title' => t('Administer Decibel Insight'),
      'description' => t('Insert Decibel Insight integration code into the head of the page based on path selection.'),
      'restrict access' => TRUE,
    ),
  );
}


/**
 * Argument load handler for %decibel_insight_integration_profile URL placeholders
 */
function decibel_insight_integration_profile_load($arg) {
  $settings = variable_get('decibel_insight_integration_profiles', array());
  return isset($settings[$arg]) ? $settings[$arg] : FALSE;
}


/**
 * Implements hook_process_html().
 * This is used to inject any code onto the appropriate pages.
 */
function decibel_insight_integration_process_html(&$vars) {
  $settings = variable_get('decibel_insight_integration_profiles', array());

  $output = array();

  foreach ($settings as $profile) {
    if (empty($profile['paths'])) {
      $page_match = TRUE;
    }
    else {
      // NOTE: This code is "borrowed" from block_list().
      $path = drupal_get_path_alias($_GET['q']);

      // Compare with the internal and path alias (if any).
      $page_match = drupal_match_path($path, $profile['paths']);
      if ($path != $_GET['q']) {
        $page_match = $page_match || drupal_match_path($_GET['q'], $profile['paths']);
      }

      // When $profile['path_visibility'] has a value of 0, the block is displayed on
      // all pages except those listed in $block->pages. When set to 1, it
      // is displayed only on those pages listed in $block->pages.
      $page_match = !($profile['path_visibility'] xor $page_match);
    }
    
    $code = "<!-- Decibel Insight -->
<script type=\"text/javascript\">
	var _da_=_da_||[];_da_.oldErr=window.onerror;_da_.err=[];_da_.q=[];
		window.onerror=function(e){_da_.err.push(e);_da_.oldErr&&_da_.oldErr(e);};
	(function(De,ci,bel, In,si,gh,t) {
		De['DecibelInsight']=si;De[si]=De[si]||function(){(De[si].q=De[si].q||[]).push(arguments);};
		gh=ci.createElement(bel),t=ci.getElementsByTagName(bel)[0];gh.async=1;gh.src=In;t.parentNode.insertBefore(gh,t);
        })(window,document,'script','//decibelinsight.net/i/{$profile['websiteId']}/di.js','decibelInsight');
</script>";

    if ($page_match) {
      $output[] = array(
        '#markup' => $code,
        '#suffix' => "\n",
      );
    }
  }

  if (!empty($output)) {
    $vars['head'] .= drupal_render($output);
  }
}
