<?php

/**
 * @file
 * This file contains all the admin-related callbacks
 * 
 */


/**
 * Overview page - provides a list of existing Decibel Insight profiles
 */
function decibel_insight_integration_overview() {
  $settings = variable_get('decibel_insight_integration_profiles', array());

  $rows = array();
  $header = array(t('Title'), t('Website ID'), t('Paths'), t('Visibility'), t('Ops'));


  foreach ($settings as $delta => $settings) {
    $rows[] = array(
      check_plain($settings['name']),
      check_plain($settings['websiteId']),
      nl2br(check_plain($settings['paths'])),
      ($settings['path_visibility'] == 0 ? t('Exclude') : t('Include')),
      implode(' | ', array(
        l(t('Edit'), 'admin/config/development/Decibel-Insight/' . $delta),
        l(t('Delete'), 'admin/config/development/Decibel-Insight/' . $delta . '/delete'),
      )),
    );
  }

  if (!empty($rows)) {
    $rows[] = array(array(
      'colspan' => count($header),
      'data' => l(t('Add another profile'), 'admin/config/development/Decibel-Insight/add'),
    ));
  }

  return array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No profiles configured yet. !link', array('!link' => l(t('Add one now'), 'admin/config/development/Decibel-Insight/add')))
  );
}


/**
 * This function provides the edit form.
 * The Add Profile form also uses this.
 * @see decibel_insight_integration_forms()
 */
function decibel_insight_integration_edit_profile($form, &$form_state, $profile = array()) {
  // Fill in profile defaults to ensure all keys exist.
  $profile += array(
    'name' => '',
    'websiteId' => '',
    'path_visibility' => '',
    'paths' => '',
  );

  $form['name_orig'] = array(
    '#type' => 'value',
    '#value' => $profile['name'],
  );

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('This is the unique name for this profile'),
    '#required' => TRUE,
    '#default_value' => $profile['name'],
  );

  $form['websiteId'] = array(
    '#type' => 'textfield',
    '#title' => t('Website ID'),
    '#description' => t('Enter your Decibel Insight website ID, this can be found in the App under "Settings"'),
    '#required' => TRUE,
    '#default_value' => $profile['websiteId'],
  );


  $form['path_visibility'] = array(
    '#type' => 'radios',
    '#title' => t('Embed code on specific pages'),
    '#options' => array(
      0 => t('Track every page except the listed pages.'),
      1 => t('Track only the listed pages.'),
    ),
    '#default_value' => $profile['path_visibility'],
  );

  $form['paths'] = array(
    '#type' => 'textarea',
    '#title' => t('Paths'),
    '#description' => t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>')),
    '#required' => FALSE,
    '#default_value' => $profile['paths'],
    '#wysiwyg' => FALSE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}


/**
 * Validate handler for the add/edit form
 */
function decibel_insight_integration_edit_profile_validate($form, &$form_state) {
  $settings = variable_get('decibel_insight_integration_profiles', array());

  if (preg_match('/[^a-z0-9\-]/', $form_state['values']['name'])) {
    form_set_error('name', t('The name should only contain lower case letters, numbers and hyphens.'));
  }
  elseif ( ($form_state['values']['name'] != $form_state['values']['name_orig']) && (isset($settings[$form_state['values']['name']])) ) {
    form_set_error('name', t('This name has already been used. Please try another.'));
  }

  if (preg_match('/[^0-9]/', $form_state['values']['websiteId'])) {
    form_set_error('name', t('The Website ID should be a number.'));
  }
}


/**
 * Submit handler for the add/edit form
 */
function decibel_insight_integration_edit_profile_submit($form, &$form_state) {
  $settings = variable_get('decibel_insight_integration_profiles', array());

  if ($form_state['values']['name'] != $form_state['values']['name_orig']) {
    unset($settings[$form_state['values']['name_orig']]);
  }

  $settings[$form_state['values']['name']] = array(
    'name'  => $form_state['values']['name'],
    'websiteId'  => trim($form_state['values']['websiteId']),
    'paths' => trim($form_state['values']['paths']),
    'path_visibility' => $form_state['values']['path_visibility'],
  );

  variable_set('decibel_insight_integration_profiles', $settings);

  $form_state['redirect'] = 'admin/config/development/Decibel-Insight';
}


/**
 * Delete confirm form for removing a profile
 */
function decibel_insight_integration_delete_profile_confirm($form, &$form_state, $profile) {
  $form['#profile'] = $profile;

  return confirm_form($form,
    t('Are you sure you want to delete the profile %name', array('%name' => $profile['name'])),
    'admin/config/development/Decibel-Insight',
    t('This cannot be undone'),
    t('Delete'), t('Cancel')
  );
}


/**
 * Delete confirm form submit handler for removing a profile.
 */
function decibel_insight_integration_delete_profile_confirm_submit($form, &$form_state) {
  $settings = variable_get('decibel_insight_integration_profiles', array());

  unset($settings[$form['#profile']['name']]);

  variable_set('decibel_insight_integration_profiles', $settings);

  $form_state['redirect'] = 'admin/config/development/Decibel-Insight';
}
